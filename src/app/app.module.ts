import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ComponentsModule } from "./modules/components/components.module";
import { PipesModule } from "./shared/pipes/pipes.module";
import { EmpleadosModule } from "./modules/empleados/empleados.module";
import { TipoDocumentoModule } from "./modules/tipo-documento/tipo-documento.module";
import { AreaModule } from "./modules/area/area.module";
import { PaisModule } from "./modules/pais/pais.module";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ComponentsModule,
    PipesModule,
    EmpleadosModule,
    AreaModule,
    PaisModule,
    TipoDocumentoModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
