import { Component, OnInit } from "@angular/core";
import { EmpleadosService } from "src/app/services/empleados.service";
import { EmpleadoModel } from "src/app/shared/models/empleado-model";

@Component({
  selector: "app-lista-empleados",
  templateUrl: "./lista-empleados.component.html",
  styleUrls: ["./lista-empleados.component.scss"],
})
export class ListaEmpleadosComponent implements OnInit {
  public empleados?: EmpleadoModel[];
  constructor(private _empledosService: EmpleadosService) {}

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    this._empledosService.getList(0).subscribe(
      (res: EmpleadoModel[]) => {
        this.empleados = res;
      },
      (err) => {
        console.error({ err });
      }
    );
  }

  onNew() {
    this._empledosService.routeNew();
  }
}
