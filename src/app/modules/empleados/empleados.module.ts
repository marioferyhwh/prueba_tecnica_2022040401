import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListaEmpleadosComponent } from "./lista-empleados/lista-empleados.component";
import { EditarEmpleadosComponent } from "./editar-empleados/editar-empleados.component";
import { CrearEmpleadosComponent } from "./crear-empleados/crear-empleados.component";
import { ComponentsModule } from "../components/components.module";

const declarations = [
  ListaEmpleadosComponent,
  EditarEmpleadosComponent,
  CrearEmpleadosComponent,
];
@NgModule({
  declarations: [...declarations],
  imports: [CommonModule,ComponentsModule],
  exports: [...declarations],
})
export class EmpleadosModule {}
