import { Component, OnInit } from "@angular/core";
import { EmpleadosService } from "src/app/services/empleados.service";
import { EmpleadoModel } from "src/app/shared/models/empleado-model";
import Swal from "sweetalert2";

@Component({
  selector: "app-crear-empleados",
  templateUrl: "./crear-empleados.component.html",
  styleUrls: ["./crear-empleados.component.scss"],
})
export class CrearEmpleadosComponent implements OnInit {
  public empleado?: EmpleadoModel;
  constructor(private _empleadoService: EmpleadosService) {}

  ngOnInit(): void {
    this.empleado = new EmpleadoModel();
  }
  onCreate(data: EmpleadoModel) {
    const toast = Swal.mixin({
      allowOutsideClick: false,
      text: "espere por favor",
      icon: "info",
    });
    toast.fire();
    toast.showLoading();
    console.log(data);
    this._empleadoService.create(data).subscribe(
      (resp) => {
        toast.close();
        const toast2 = Swal.mixin({
          title: "empleado creado",
          text: "",
          icon: "success",
        });
        toast2.fire();
        this._empleadoService.routeList();
      },
      (err) => {
        const toast = Swal.mixin({
          title: "error al crear empleado",
          text: err["error"].message,
          icon: "error",
        });
        toast.fire();
      }
    );
  }
}
