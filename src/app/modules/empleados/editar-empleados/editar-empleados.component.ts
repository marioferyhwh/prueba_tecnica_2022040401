import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { EmpleadosService } from "src/app/services/empleados.service";
import { EmpleadoModel } from "src/app/shared/models/empleado-model";
import Swal from "sweetalert2";

@Component({
  selector: "app-editar-empleados",
  templateUrl: "./editar-empleados.component.html",
  styleUrls: ["./editar-empleados.component.scss"],
})
export class EditarEmpleadosComponent implements OnInit {
  public empleado?: EmpleadoModel;
  constructor(
    private _empleadoService: EmpleadosService,
    private _activedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this._activedRoute.params.subscribe((params) => {
      console.log(params);
      this._empleadoService.get(params["id"]).subscribe(
        (res) => {
          console.log({ res });
          this.empleado = res;
        },
        (err) => {
          const toast2 = Swal.mixin({
            title: "empleado no secontredo",
            text: "",
            icon: "info",
          });
          toast2.fire();
          console.log(err.error);
          this._empleadoService.routeList();
        }
      );
    });
  }

  onUpdate(c: EmpleadoModel) {
    console.log({ c });
    const toast = Swal.mixin({
      allowOutsideClick: false,
      text: "espere por favor",
      icon: "info",
    });
    toast.fire();
    toast.showLoading();
    this._empleadoService.edit(c).subscribe(
      (resp) => {
        toast.close();
        const toast2 = Swal.mixin({
          title: "empleado editado",
          text: "",
          icon: "success",
        });
        toast2.fire();
        this._empleadoService.routeList();
      },
      (err) => {
        toast.close();
        const toast2 = Swal.mixin({
          title: "error",
          text: err.error.message,
          icon: "error",
        });
        toast2.fire();
        console.log({ err });
      }
    );
  }
}
