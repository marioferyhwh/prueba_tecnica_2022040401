import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListTipoDocumentoComponent } from "./list-tipo-documento/list-tipo-documento.component";
import { ComponentsModule } from "../components/components.module";

const declarations = [ListTipoDocumentoComponent];
@NgModule({
  declarations: [...declarations],
  imports: [CommonModule,ComponentsModule],
  exports: [...declarations],
})
export class TipoDocumentoModule {}
