import { Component, OnInit } from "@angular/core";
import { TipoDocumentoService } from "src/app/services/tipo-documento.service";
import { TipoDocumentoModel } from "src/app/shared/models/tipo-documento-model";

@Component({
  selector: "app-list-tipo-documento",
  templateUrl: "./list-tipo-documento.component.html",
  styleUrls: ["./list-tipo-documento.component.scss"],
})
export class ListTipoDocumentoComponent implements OnInit {
  public listTipoDocument?: TipoDocumentoModel[];
  constructor(private _tipoService: TipoDocumentoService) {}

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this._tipoService.getList(0).subscribe(
      (res: TipoDocumentoModel[]) => {
        this.listTipoDocument = res;
      },
      (err) => {
        console.error({ err });
      }
    );
  }
}
