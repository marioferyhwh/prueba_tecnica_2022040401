import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { AreaService } from "src/app/services/area.service";
import { EmpleadosService } from "src/app/services/empleados.service";
import { PaisService } from "src/app/services/pais.service";
import { TipoDocumentoService } from "src/app/services/tipo-documento.service";
import { AreaModel } from "src/app/shared/models/area-model";
import { EmpleadoModel } from "src/app/shared/models/empleado-model";
import { PaisModel } from "src/app/shared/models/pais-model";
import { TipoDocumentoModel } from "src/app/shared/models/tipo-documento-model";
import Swal from "sweetalert2";

@Component({
  selector: "app-table-empleado",
  templateUrl: "./table-empleado.component.html",
  styleUrls: ["./table-empleado.component.scss"],
})
export class TableEmpleadoComponent implements OnInit {
  @Input() data?: EmpleadoModel[];
  @Input() edit: boolean = false;
  @Output() onReload: EventEmitter<string>;

  paises: PaisModel[] = [];
  tipos: TipoDocumentoModel[] = [];
  areas: AreaModel[] = [];
  constructor(
    private _empleadoService: EmpleadosService,
    private _paisService: PaisService,
    private _tipoDocService: TipoDocumentoService,
    private _areaService: AreaService
  ) {
    this.edit = false;
    this.onReload = new EventEmitter();

    this._paisService.getList(0).subscribe(
      (res: PaisModel[]) => {
        this.paises = res;
      },
      (err) => {
        console.error({ err });
      }
    );
    this._areaService.getList(0).subscribe(
      (res: AreaModel[]) => {
        this.areas = res;
      },
      (err) => {
        console.error({ err });
      }
    );
    this._tipoDocService.getList(0).subscribe(
      (res: TipoDocumentoModel[]) => {
        this.tipos = res;
      },
      (err) => {
        console.error({ err });
      }
    );
  }

  ngOnInit(): void {}

  selectItem(id: number) {
    this._empleadoService.routeSee(id);
  }
  editItem(id: number) {
    this._empleadoService.routeEdit(id);
  }
  deleteItem(id: number) {
    const toast = Swal.mixin({
      title: "SEGURO",
      text: "no se podra revertir",
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Si, borrar!",
      cancelButtonText: "No, cancel!",
      reverseButtons: true,
    });
    toast.fire().then((result) => {
      if (result.value) {
        this._empleadoService.delete(id).subscribe(
          (resp) => {
            const toast = Swal.mixin({
              title: "BORRADO",
              text: "empleado borrado",
              icon: "success",
            });
            toast.fire();
            this.onReload.emit("");
            console.log(resp);
          },
          (err) => {
            console.log(err);
          }
        );
      } else {
        const toast = Swal.mixin({
          title: "CANCELADO",
          text: "empleado nose eliminara",
          icon: "error",
        });
        toast.fire();
      }
    });
  }
}
