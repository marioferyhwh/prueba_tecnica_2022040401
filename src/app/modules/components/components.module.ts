import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { FormEmpleadoComponent } from "./form-empleado/form-empleado.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { TableEmpleadoComponent } from "./table-empleado/table-empleado.component";
import { LoadingComponent } from "./loading/loading.component";
import { ButtonComponent } from "./button/button.component";
import { ButtonDeleteComponent } from "./button-delete/button-delete.component";
import { ButtonEditComponent } from "./button-edit/button-edit.component";
import { ButtonNewComponent } from "./button-new/button-new.component";
import { ButtonSeeComponent } from "./button-see/button-see.component";
import { ButtonCreateEditComponent } from "./button-create-edit/button-create-edit.component";
import { TableTipoComponent } from "./table-tipo/table-tipo.component";
import { AppRoutingModule } from "src/app/app-routing.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { PipesModule } from "src/app/shared/pipes/pipes.module";

const declarations = [
  HeaderComponent,
  FooterComponent,
  FormEmpleadoComponent,
  NavbarComponent,
  TableEmpleadoComponent,
  LoadingComponent,
  ButtonComponent,
  ButtonDeleteComponent,
  ButtonEditComponent,
  ButtonNewComponent,
  ButtonSeeComponent,
  ButtonCreateEditComponent,
  TableTipoComponent,
];
@NgModule({
  declarations: [...declarations],
  imports: [
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule,
  ],
  exports: [...declarations],
})
export class ComponentsModule {}
