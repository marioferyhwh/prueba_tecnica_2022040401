import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableTipoComponent } from './table-tipo.component';

describe('TableTipoComponent', () => {
  let component: TableTipoComponent;
  let fixture: ComponentFixture<TableTipoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableTipoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableTipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
