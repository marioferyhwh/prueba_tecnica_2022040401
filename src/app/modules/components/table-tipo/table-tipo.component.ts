import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "app-table-tipo",
  templateUrl: "./table-tipo.component.html",
  styleUrls: ["./table-tipo.component.scss"],
})
export class TableTipoComponent implements OnInit {
  @Input() data?: any[];
  @Input() data_id: string="";
  @Input() data_name: string="";
  constructor() {}

  ngOnInit(): void {}
}
