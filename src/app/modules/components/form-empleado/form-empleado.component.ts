import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AreaService } from "src/app/services/area.service";
import { EmpleadosService } from "src/app/services/empleados.service";
import { PaisService } from "src/app/services/pais.service";
import { TipoDocumentoService } from "src/app/services/tipo-documento.service";
import { AreaModel } from "src/app/shared/models/area-model";
import { EmpleadoModel } from "src/app/shared/models/empleado-model";
import { PaisModel } from "src/app/shared/models/pais-model";
import { TipoDocumentoModel } from "src/app/shared/models/tipo-documento-model";

@Component({
  selector: "app-form-empleado",
  templateUrl: "./form-empleado.component.html",
  styleUrls: ["./form-empleado.component.scss"],
})
export class FormEmpleadoComponent implements OnInit {
  @Input() public data?: EmpleadoModel;
  @Output() public onData: EventEmitter<EmpleadoModel>;
  paises: PaisModel[] = [];
  tipos: TipoDocumentoModel[] = [];
  areas: AreaModel[] = [];
  public forma: FormGroup;
  public debug: boolean;
  constructor(
    private _fb: FormBuilder,
    private _empleadoService: EmpleadosService,
    private _paisService: PaisService,
    private _tipoDocService: TipoDocumentoService,
    private _areaService: AreaService
  ) {
    this.debug = false;
    this.onData = new EventEmitter();
    this.forma = this._fb.group({});
    this.initForm();
  }
  ngOnInit(): void {}
  ngOnChanges() {
    this.dataForm();
  }
  onAction() {
    if (this.forma.invalid) {
      console.log(this.forma);
      Object.values(this.forma.controls).forEach((c) => {
        if (c instanceof FormGroup) {
          Object.values(c.controls).forEach((c) => {
            c.markAsTouched();
          });
        } else {
          c.markAsTouched();
        }
      });
      return;
    }
    const d = <EmpleadoModel>this.forma.value;
    d.empleado_codigo = (<EmpleadoModel>this.data).empleado_codigo;
    console.log({ d });
    this.onData.emit(d);
  }
  initForm() {
    this._paisService.getList(0).subscribe(
      (res: PaisModel[]) => {
        this.paises = res;
      },
      (err) => {
        console.error({ err });
      }
    );
    this._areaService.getList(0).subscribe(
      (res: AreaModel[]) => {
        this.areas = res;
      },
      (err) => {
        console.error({ err });
      }
    );
    this._tipoDocService.getList(0).subscribe(
      (res: TipoDocumentoModel[]) => {
        this.tipos = res;
      },
      (err) => {
        console.error({ err });
      }
    );

    this.forma = this._fb.group({
      empleado_codigo: [0],
      empleado_primer_nombre: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
      ],
      empleado_otro_nombre: ["", [Validators.maxLength(50)]],
      empleado_primer_apellido: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
      ],
      empleado_segundo_apellido: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
      ],
      empleado_identificacion: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(20),
        ],
      ],
      empleado_correo: [
        "",
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(300),
        ],
      ],
      empleado_estado: [false],
      empleado_ingreso: [""],
      empleado_creado: [""],
      empleado_modificado: [""],
      pais_codigo: ["0"],
      tipodocumento_codigo: ["0"],
      area_codigo: ["0"],
    });
  }

  dataForm() {
    this.forma.reset({ ...this.data });
    this.forma.get("empleado_codigo")?.disable();
    this.forma.get("empleado_creado")?.disable();
    this.forma.get("empleado_modificado")?.disable();
    this.forma.get("empleado_correo")?.disable();
  }
  cancel() {
    this._empleadoService.routeList();
  }

  InvalidField(Field: string): boolean {
    const value = this.forma?.get(Field);
    if (value) {
      return value.invalid && value.touched;
    }
    return true;
  }

  get codigoInvalid(): boolean {
    return this.InvalidField("empleado_codigo");
  }
  get primer_nombreInvalid(): boolean {
    return this.InvalidField("empleado_primer_nombre");
  }
  get otro_nombreInvalid(): boolean {
    return this.InvalidField("empleado_otro_nombre");
  }
  get primer_apellidoInvalid(): boolean {
    return this.InvalidField("empleado_primer_apellido");
  }
  get segundo_apellidoInvalid(): boolean {
    return this.InvalidField("empleado_segundo_apellido");
  }
  get identificacionInvalid(): boolean {
    return this.InvalidField("empleado_identificacion");
  }
  get correoInvalid(): boolean {
    return this.InvalidField("empleado_correo");
  }
  get ingresoInvalid(): boolean {
    return this.InvalidField("empleado_ingreso");
  }
  get estadoInvalid(): boolean {
    return this.InvalidField("empleado_estado");
  }
  get creadoInvalid(): boolean {
    return this.InvalidField("empleado_creado");
  }
  get modificadoInvalid(): boolean {
    return this.InvalidField("empleado_modificado");
  }
  get paisInvalid(): boolean {
    return this.InvalidField("pais_codigo");
  }
  get areaInvalid(): boolean {
    return this.InvalidField("area_codigo");
  }
  get tipodocumentoInvalid(): boolean {
    return this.InvalidField("tipodocumento_codigo");
  }
}
