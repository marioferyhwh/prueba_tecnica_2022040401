import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListPaisComponent } from "./list-pais/list-pais.component";
import { ComponentsModule } from "../components/components.module";

const declarations = [ListPaisComponent];
@NgModule({
  declarations: [...declarations],
  imports: [CommonModule, ComponentsModule],
  exports: [...declarations],
})
export class PaisModule {}
