import { Component, OnInit } from "@angular/core";
import { PaisService } from "src/app/services/pais.service";
import { PaisModel } from "src/app/shared/models/pais-model";

@Component({
  selector: "app-list-pais",
  templateUrl: "./list-pais.component.html",
  styleUrls: ["./list-pais.component.scss"],
})
export class ListPaisComponent implements OnInit {
  public listPaises?: PaisModel[];
  constructor(private _paisService: PaisService) {}

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this._paisService.getList(0).subscribe(
      (res: PaisModel[]) => {
        this.listPaises = res;
      },
      (err) => {
        console.error({ err });
      }
    );
  }
}
