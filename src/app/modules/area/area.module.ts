import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ListAreaComponent } from "./list-area/list-area.component";
import { ComponentsModule } from "../components/components.module";

const declarations = [ListAreaComponent];
@NgModule({
  declarations: [...declarations],
  imports: [CommonModule, ComponentsModule],
  exports: [...declarations],
})
export class AreaModule {}
