import { Component, OnInit } from '@angular/core';
import { AreaService } from 'src/app/services/area.service';
import { AreaModel } from 'src/app/shared/models/area-model';

@Component({
  selector: 'app-list-area',
  templateUrl: './list-area.component.html',
  styleUrls: ['./list-area.component.scss']
})
export class ListAreaComponent implements OnInit {


  public listAreas?: AreaModel[];
  constructor(private _areaService: AreaService) {}

  ngOnInit(): void {
    this.getData();
  }
  getData() {
    this._areaService.getList(0).subscribe(
      (res: AreaModel[]) => {
        this.listAreas = res;
      },
      (err) => {
        console.error({ err });
      }
    );
  }

}
