import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ListAreaComponent } from "./modules/area/list-area/list-area.component";
import { CrearEmpleadosComponent } from "./modules/empleados/crear-empleados/crear-empleados.component";
import { EditarEmpleadosComponent } from "./modules/empleados/editar-empleados/editar-empleados.component";
import { ListaEmpleadosComponent } from "./modules/empleados/lista-empleados/lista-empleados.component";
import { ListPaisComponent } from "./modules/pais/list-pais/list-pais.component";
import { ListTipoDocumentoComponent } from "./modules/tipo-documento/list-tipo-documento/list-tipo-documento.component";

const routes: Routes = [
  { path: "empleados", component: ListaEmpleadosComponent },
  { path: "empleados/nuevo", component: CrearEmpleadosComponent },
  { path: "empleados/:id", component: EditarEmpleadosComponent },
  { path: "empleados/:id/editar", component: EditarEmpleadosComponent },
  { path: "paises", component: ListPaisComponent },
  { path: "areas", component: ListAreaComponent },
  { path: "documentos", component: ListTipoDocumentoComponent },
  { path: "**", pathMatch: "full", redirectTo: "empleados" },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
