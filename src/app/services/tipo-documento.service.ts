import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable } from "rxjs";
import { RespModel } from "../shared/models/resp-model";
import { TipoDocumentoModel } from "../shared/models/tipo-documento-model";
import { ApiGlobalService } from "./api-global.service";
import { ApiServerService } from "./api-server.service";

@Injectable({
  providedIn: "root",
})
export class TipoDocumentoService {
  private _urlA: String = "tipodocumento";
  constructor(
    private _api: ApiServerService,
    private _router: Router,
    private _global: ApiGlobalService
  ) {}

  private tipoDocumentoTest: TipoDocumentoModel[] = [
    new TipoDocumentoModel(1, "cedula de ciudadania"),
    new TipoDocumentoModel(2, "cedula de estrangeria"),
    new TipoDocumentoModel(3, "pasaporte"),
    new TipoDocumentoModel(4, "permiso especial"),
  ];

  getList(c: number): Observable<TipoDocumentoModel[]> {
    const tipodocumento = this._global.varTipoDocument;
    if (tipodocumento.length > 0) {
      return new Observable((obs) => {
        obs.next(tipodocumento);
      });
    }
    if (false) {
      return new Observable((obs) => {
        this._global.varTipoDocument = <Array<TipoDocumentoModel>>(
          this.tipoDocumentoTest
        );
        obs.next(this.tipoDocumentoTest);
      });
    }
    return this._api.GetQuery(`${this._urlA}`).pipe(
      map((data: RespModel) => {
        this._global.varTipoDocument = <Array<TipoDocumentoModel>>data.data;
        return <Array<TipoDocumentoModel>>data.data;
      })
    );
  }

  get(id: number): Observable<TipoDocumentoModel> {
    return this._api.GetQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <TipoDocumentoModel>data.data;
      })
    );
  }

  create(tipodocumento: TipoDocumentoModel): Observable<TipoDocumentoModel> {
    return this._api.PostQuery(`${this._urlA}`, tipodocumento).pipe(
      map((data: RespModel) => {
        return <TipoDocumentoModel>data.data;
      })
    );
  }

  edit(tipodocumento: TipoDocumentoModel): Observable<TipoDocumentoModel> {
    let id = tipodocumento.tipodocumento_codigo;
    return this._api.PutQuery(`${this._urlA}/${id}`, tipodocumento).pipe(
      map((data: RespModel) => {
        return <TipoDocumentoModel>data.data;
      })
    );
  }

  delete(id: number): Observable<TipoDocumentoModel> {
    return this._api.DeleteQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <TipoDocumentoModel>data.data;
      })
    );
  }
}
