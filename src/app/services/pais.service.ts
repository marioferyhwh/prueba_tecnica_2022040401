import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable } from "rxjs";
import { PaisModel } from "../shared/models/pais-model";
import { RespModel } from "../shared/models/resp-model";
import { ApiGlobalService } from "./api-global.service";
import { ApiServerService } from "./api-server.service";

@Injectable({
  providedIn: "root",
})
export class PaisService {
  private _urlA: String = "pais";
  constructor(
    private _api: ApiServerService,
    private _router: Router,
    private _global: ApiGlobalService
  ) {}
  private paisTest: PaisModel[] = [
    new PaisModel(1, "Colombia"),
    new PaisModel(2, "Estados Unidos"),
  ];

  getList(c: number): Observable<PaisModel[]> {
    const paises = this._global.varPaises;
    if (paises.length > 0) {
      return new Observable((obs) => {
        obs.next(paises);
      });
    }
    if (false) {
      return new Observable((obs) => {
        this._global.varPaises = <Array<PaisModel>>this.paisTest;
        obs.next(this.paisTest);
      });
    }
    return this._api.GetQuery(`${this._urlA}`).pipe(
      map((data: RespModel) => {
        this._global.varPaises = <Array<PaisModel>>data.data;
        return <Array<PaisModel>>data.data;
      })
    );
  }

  get(id: number): Observable<PaisModel> {
    return this._api.GetQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <PaisModel>data.data;
      })
    );
  }

  create(pais: PaisModel): Observable<PaisModel> {
    return this._api.PostQuery(`${this._urlA}`, pais).pipe(
      map((data: RespModel) => {
        return <PaisModel>data.data;
      })
    );
  }

  edit(pais: PaisModel): Observable<PaisModel> {
    let id = pais.pais_codigo;
    return this._api.PutQuery(`${this._urlA}/${id}`, pais).pipe(
      map((data: RespModel) => {
        return <PaisModel>data.data;
      })
    );
  }

  delete(id: number): Observable<PaisModel> {
    return this._api.DeleteQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <PaisModel>data.data;
      })
    );
  }
}
