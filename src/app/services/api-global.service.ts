import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { AreaModel } from "../shared/models/area-model";
import { GlobalModel } from "../shared/models/global-model";
import { PaisModel } from "../shared/models/pais-model";
import { TipoDocumentoModel } from "../shared/models/tipo-documento-model";
import { ApiServerService } from "./api-server.service";

@Injectable({
  providedIn: "root",
})
export class ApiGlobalService {
  private _global: GlobalModel;

  constructor() {
    this._global = new GlobalModel([], [], []);
    this.varAllSave();
  }
  private _nameVar = "api-var";

  getVar(): void {
    const tempVar = sessionStorage.getItem(this._nameVar);
    if (tempVar) {
      this._global = JSON.parse(tempVar);
    }
  }

  varAllSave() {
    sessionStorage.setItem(this._nameVar, JSON.stringify(this._global));
  }

  clearVar() {
    if (sessionStorage.getItem(this._nameVar)) {
      sessionStorage.removeItem(this._nameVar);
    }
    this._global = new GlobalModel([], [], []);
  }
  set varPaises(dato: PaisModel[]) {
    this._global.paises = dato;

    this.varAllSave();
  }
  get varPaises(): PaisModel[] {
    this.getVar();
    return this._global.paises;
  }
  set varArea(dato: AreaModel[]) {
    this._global.areas = dato;

    this.varAllSave();
  }
  get varArea(): AreaModel[] {
    this.getVar();
    return this._global.areas;
  }

  get varTipoDocument(): TipoDocumentoModel[] {
    this.getVar();
    return this._global.tipo_documentos;
  }

  set varTipoDocument(dato: TipoDocumentoModel[]) {
    this._global.tipo_documentos = dato;

    this.varAllSave();
  }
}
