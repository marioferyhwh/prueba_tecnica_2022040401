import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable } from "rxjs";
import { EmpleadoModel } from "../shared/models/empleado-model";
import { RespModel } from "../shared/models/resp-model";
import { ApiGlobalService } from "./api-global.service";
import { ApiServerService } from "./api-server.service";

@Injectable({
  providedIn: "root",
})
export class EmpleadosService {
  private _urlA: String = "empleado";
  constructor(
    private _api: ApiServerService,
    private _router: Router,
    private _global: ApiGlobalService
  ) {}

  private emp: EmpleadoModel = {
    empleado_codigo: 1,
    empleado_primer_nombre: "",
    empleado_otro_nombre: "",
    empleado_primer_apellido: "",
    empleado_segundo_apellido: "",
    pais_codigo: 1,
    tipodocumento_codigo: 1,
    empleado_identificacion: "",
    empleado_correo: "",
    area_codigo: 1,
    empleado_estado: true,
    empleado_ingreso: "",
    empleado_creado: "",
    empleado_modificado: "",
  };
  private empledoTest: EmpleadoModel[] = [
    {
      empleado_codigo: 1,
      empleado_primer_nombre: "nombre_1",
      empleado_otro_nombre: "",
      empleado_primer_apellido: "apellido1_1",
      empleado_segundo_apellido: "apellido2_1",
      pais_codigo: 1,
      tipodocumento_codigo: 1,
      empleado_identificacion: "111111",
      empleado_correo: "@correo.com",
      area_codigo: 1,
      empleado_estado: true,
      empleado_ingreso: "2021/01/01",
      empleado_creado: "2021/01/01",
      empleado_modificado: "2021/01/01",
    },
    {
      empleado_codigo: 2,
      empleado_primer_nombre: "nombre_2",
      empleado_otro_nombre: "",
      empleado_primer_apellido: "apellido1_2",
      empleado_segundo_apellido: "apellido2_2",
      pais_codigo: 1,
      tipodocumento_codigo: 1,
      empleado_identificacion: "111111",
      empleado_correo: "@correo.com",
      area_codigo: 1,
      empleado_estado: true,
      empleado_ingreso: "2021/01/01",
      empleado_creado: "2021/01/01",
      empleado_modificado: "2021/01/01",
    },
    {
      empleado_codigo: 3,
      empleado_primer_nombre: "nombre_3",
      empleado_otro_nombre: "",
      empleado_primer_apellido: "apellido1_3",
      empleado_segundo_apellido: "apellido2_3",
      pais_codigo: 1,
      tipodocumento_codigo: 1,
      empleado_identificacion: "111111",
      empleado_correo: "@correo.com",
      area_codigo: 1,
      empleado_estado: true,
      empleado_ingreso: "2021/01/01",
      empleado_creado: "2021/01/01",
      empleado_modificado: "2021/01/01",
    },
  ];
  getList(c: number): Observable<EmpleadoModel[]> {
    if (false) {
      return new Observable((obs) => {
        obs.next(this.empledoTest);
      });
    }
    return this._api.GetQuery(`${this._urlA}`).pipe(
      map((data: RespModel) => {
        return <Array<EmpleadoModel>>data.data;
      })
    );
  }

  get(id: number): Observable<EmpleadoModel> {
    if (false) {
      return new Observable((obs) => {
        obs.next(this.empledoTest.find((x) => x.empleado_codigo == id));
      });
    }
    return this._api.GetQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <EmpleadoModel>data.data;
      })
    );
  }

  create(empleado: EmpleadoModel): Observable<EmpleadoModel> {
    return this._api.PostQuery(`${this._urlA}`, empleado).pipe(
      map((data: RespModel) => {
        return <EmpleadoModel>data.data;
      })
    );
  }

  edit(empleado: EmpleadoModel): Observable<EmpleadoModel> {
    let id = empleado.empleado_codigo;
    return this._api.PutQuery(`${this._urlA}/${id}`, empleado).pipe(
      map((data: RespModel) => {
        return <EmpleadoModel>data.data;
      })
    );
  }

  delete(id: number): Observable<EmpleadoModel> {
    return this._api.DeleteQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <EmpleadoModel>data.data;
      })
    );
  }

  routeList() {
    this._router.navigate(["/empleados"]);
  }
  routeNew() {
    this._router.navigate(["/empleados", "nuevo"]);
  }
  routeEdit(id: number) {
    this._router.navigate(["/empleados", id, "editar"]);
  }
  routeSee(id: number) {
    this._router.navigate(["/empleados", id]);
  }
}
