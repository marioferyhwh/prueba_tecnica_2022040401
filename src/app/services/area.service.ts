import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable } from "rxjs";
import { AreaModel } from "../shared/models/area-model";
import { RespModel } from "../shared/models/resp-model";
import { ApiGlobalService } from "./api-global.service";
import { ApiServerService } from "./api-server.service";

@Injectable({
  providedIn: "root",
})
export class AreaService {
  private _urlA: String = "area";
  constructor(
    private _api: ApiServerService,
    private _router: Router,
    private _global: ApiGlobalService
  ) {}

  private areaTest: AreaModel[] = [
    new AreaModel(1, "Administracion"),
    new AreaModel(2, "Financiera"),
    new AreaModel(3, "Compras"),
    new AreaModel(4, "Infraestructura"),
    new AreaModel(5, "Operacion"),
    new AreaModel(6, "Talento Humano"),
    new AreaModel(7, "Servicios Varios"),
  ];

  getList(c: number): Observable<AreaModel[]> {
    const area = this._global.varArea;
    if (area.length > 0) {
      return new Observable((obs) => {
        obs.next(area);
      });
    }
    if (false) {
      return new Observable((obs) => {
        this._global.varArea = <Array<AreaModel>>this.areaTest;
        obs.next(this.areaTest);
      });
    }
    return this._api.GetQuery(`${this._urlA}`).pipe(
      map((data: RespModel) => {
        this._global.varArea = <Array<AreaModel>>data.data;
        return <Array<AreaModel>>data.data;
      })
    );
  }

  get(id: number): Observable<AreaModel> {
    return this._api.GetQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <AreaModel>data.data;
      })
    );
  }

  create(area: AreaModel): Observable<AreaModel> {
    return this._api.PostQuery(`${this._urlA}`, area).pipe(
      map((data: RespModel) => {
        return <AreaModel>data.data;
      })
    );
  }

  edit(area: AreaModel): Observable<AreaModel> {
    let id = area.area_codigo;
    return this._api.PutQuery(`${this._urlA}/${id}`, area).pipe(
      map((data: RespModel) => {
        return <AreaModel>data.data;
      })
    );
  }

  delete(id: number): Observable<AreaModel> {
    return this._api.DeleteQuery(`${this._urlA}/${id}`).pipe(
      map((data: RespModel) => {
        return <AreaModel>data.data;
      })
    );
  }
}
