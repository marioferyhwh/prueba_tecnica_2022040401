import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ActivedPipe } from "./actived.pipe";
import { ImgNofoundPipe } from "./img-nofound.pipe";
import { SelectTipoPipe } from "./select-tipo.pipe";

const declarations = [ActivedPipe, ImgNofoundPipe, SelectTipoPipe];
@NgModule({
  declarations: [...declarations],
  imports: [CommonModule],
  exports: [...declarations],
})
export class PipesModule {}
