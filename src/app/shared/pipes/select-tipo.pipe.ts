import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "selectTipo",
})
export class SelectTipoPipe implements PipeTransform {
  transform(value: number, name: string, list: object[]): string {
    const listTemp: any = list.find((d: any) => {
      return d[name + "_codigo"] == value;
    });
    if (!listTemp) {
      return "";
    }
    return <string>listTemp[name + "_nombre"];
  }
}
