import { AreaModel } from "./area-model";
import { PaisModel } from "./pais-model";
import { TipoDocumentoModel } from "./tipo-documento-model";

export class GlobalModel {
  constructor(
    public paises: PaisModel[],
    public tipo_documentos: TipoDocumentoModel[],
    public areas: AreaModel[]
  ) {}
}
