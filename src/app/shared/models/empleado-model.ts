export class EmpleadoModel {
  public empleado_codigo: number;
  public empleado_primer_nombre: string;
  public empleado_otro_nombre?: string;
  public empleado_primer_apellido: string;
  public empleado_segundo_apellido: string;
  public pais_codigo: number;
  public tipodocumento_codigo: number;
  public empleado_identificacion: string;
  public empleado_correo: string;
  public area_codigo: number;
  public empleado_estado: boolean;
  public empleado_ingreso: string;
  public empleado_creado: string;
  public empleado_modificado: string;
  constructor() {
    this.empleado_codigo = 0;
    this.empleado_primer_nombre = "";
    this.empleado_otro_nombre = "";
    this.empleado_primer_apellido = "";
    this.empleado_segundo_apellido = "";
    this.pais_codigo = 0;
    this.tipodocumento_codigo = 0;
    this.empleado_identificacion = "";
    this.empleado_correo = "";
    this.area_codigo = 0;
    this.empleado_estado = true;
    this.empleado_ingreso = "";
    this.empleado_creado = "";
    this.empleado_modificado = "";
  }
}
